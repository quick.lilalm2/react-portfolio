import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import AboutSection from "./api/about";
import SovTechSection from "./api/sovTech";
import PersonalProjectsSection from "./api/projects";

export default function Home() {
  return (
    <div className={styles.container} style={{backgroundImage: "url(/dark-textured-wallpapers.jpeg)"}}>
      <Head>
        <title>Liam Quick Portfolio</title>
        <meta name="description" content="Created by Liam Quick" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Something about <br/>
          <a target="_blank" rel="noreferrer" href="https://www.linkedin.com/in/liam-quick-764959232/">Liam Quick</a>
        </h1>

        <p className={styles.description}>
          <img className={styles.profile} src="/100-LiamQuick.jpg" alt="A picture of Liam Quick"></img>
        </p>

        <div className={styles.grid}>
          <AboutSection/>

          <SovTechSection/>

          <PersonalProjectsSection/>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}
