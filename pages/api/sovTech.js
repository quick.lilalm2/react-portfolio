
import {useState} from "react";
import styles from "../../styles/Home.module.css";

export default function SovTechSection() {
    const [isShown, setIsShown] = useState(false);

    return (
        <a className={styles.card}
           onMouseEnter={() => setIsShown(true)}
           onMouseLeave={() => setIsShown(false)}>
            <h2>Why SovTech? &rarr;</h2>
            <div>
                {!isShown &&(
                    <p>Why choose SovTech over the other tech companies?</p>
                )}
                {isShown &&(
                    <div>
                        <p>I believe that SovTech is one of the most innovative Tech Companies in South Africa.</p>
                        <p>SovTech values their employees, strives to better them and has a very friendly work culture which deeply resonates with me.</p>
                        <p></p>
                    </div>
                )}
            </div>
        </a>
    );
}
