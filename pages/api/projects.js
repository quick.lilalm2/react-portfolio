
import {useState} from "react";
import styles from "../../styles/Home.module.css";

export default function PersonalProjectsSection() {
    const [isShown, setIsShown] = useState(false);

    return (
        <a href="https://gitlab.com/quick.lilalm2" className={styles.card} target="_blank" rel="noreferrer"
           onMouseEnter={() => setIsShown(true)}
           onMouseLeave={() => setIsShown(false)}>
            <h2>Personal Projects &rarr;</h2>
            <div>
                {!isShown &&(
                    <p>Things I&apos;ve worked on in the past</p>
                )}
                {isShown &&(
                    <div>
                        <p>I created a simple Android app game in order to learn some basics on Android studio</p>
                    </div>
                )}
            </div>
        </a>
    );
}
