
import {useState} from "react";
import styles from "../../styles/Home.module.css";

export default function AboutSection() {
  const [isShown, setIsShown] = useState(false);

  return (
      <a className={styles.card}
         onMouseEnter={() => setIsShown(true)}
         onMouseLeave={() => setIsShown(false)}>
          <h2>Personal Info &rarr;</h2>
          <div>
          {!isShown &&(
                  <p>Who am I and why do I like to code?</p>
          )}
          {isShown &&(
              <div>
                  <p>My name is Liam Quick. I am currently 21 Years old.</p>
                  <p>I have loved tech since I was young because I grew up playing video games.</p>
                  <p>My first experience with coding was probably when I learnt that I could edit the game files of one of the games that I was playing.</p>
              </div>
          )}
          </div>
      </a>
  );
}
